""" General case where f(u, v) = g(u, v) - g(v, u). """

import numpy
import scipy


def objective(nrepeats):
    """ Return an objective function for nrepeats transformed data.
        It returns first and second derivatives of and anisymmetric predictor.

        y_true and y_pred are labels and model predictions
            with shape ((1 + nrepeats)*n,).

        The first n entries are original data; the second n are transformed.

        Our labels are assumed to be [1]*n + [0]*n, and ignored.
    """
    def obj(y_true, y_pred):
        preds = y_pred.reshape(2, nrepeats, -1)

        dsx = scipy.special.expit(preds[1] - preds[0])
        d2sx = dsx * (1 - dsx)

        jac = numpy.concatenate([-dsx.ravel(), dsx.ravel()])
        hess = numpy.concatenate([d2sx.ravel(), d2sx.ravel()])

        return jac, hess

    return obj


def pack(data, transform, nrepeats=1):
    """ Return packed paired data.

        -> shape (2, nrepeats, ndata, 2 * ndim)
    """
    blocks = [[], []]
    for _ in range(nrepeats):
        sdata = transform(data)
        blocks[0].append([[data, sdata]])
        blocks[1].append([[sdata, data]])
    return numpy.block(blocks)


def fit(model, packed, *args, **kwargs):
    """ Fit model to packed data.

        packed[0] is x, packed[1] is sx.

        args and kwargs are forwarded to the model.fit(...).
    """
    data = packed.reshape(-1, packed.shape[-1])
    labels = numpy.zeros(data.shape[0], bool)
    return model.fit(data, labels, *args, **kwargs)


def predict_proba(model, packed):
    """ Return probabilities at packed data. """
    _, nrepeats, _, ndim = packed.shape
    data = packed.reshape(-1, ndim)
    zetas = model.predict(data).reshape(2, nrepeats, -1)
    return scipy.special.expit(zetas[0] - zetas[1])
