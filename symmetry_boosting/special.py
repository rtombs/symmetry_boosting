""" Special case where f(u, v) = g(u) - g(v). """

import numpy
import scipy


def objective(nrepeats):
    """ Return an objective function for nrepeats transformed data.
        It returns first and second derivatives of and anisymmetric predictor.

        y_true and y_pred are labels and model predictions with
            shape ((1 + nrepeats)*ndata,).

        The first n entries are original data; the others are transformed.

        Labels are assumed to be [1]*ndata + [0]*ndata*nrepeats, and ignored.
    """
    def obj(y_true, y_pred):
        preds = y_pred.reshape(1 + nrepeats, -1)

        dsx = scipy.special.expit(preds[1:] - preds[0])
        d2sx = dsx * (1 - dsx)

        jac = numpy.concatenate([-dsx.sum(axis=0), dsx.ravel()])
        hess = numpy.concatenate([d2sx.sum(axis=0), d2sx.ravel()])

        return jac, hess

    return obj


def pack(data, transform, nrepeats=1):
    """ Return data packed with transformed versions along axis 0.

        -> shape (1 + nrepeats, ndata, ndim)
    """
    transformed = [transform(data) for _ in range(nrepeats)]
    return numpy.stack([data] + transformed)


def fit(model, packed, *args, **kwargs):
    """ Fit model to packed data.

        packed[0] is x, packed[1] is sx.

        args and kwargs are forwarded to the model.fit(...).
    """
    data = packed.reshape(-1, packed.shape[-1])
    labels = numpy.zeros(data.shape[0], bool)
    return model.fit(data, labels, *args, **kwargs)


def predict_proba(model, packed):
    """ Return probabilities at packed data. """
    ntot, _, ndim = packed.shape
    data = packed.reshape(-1, ndim)
    zetas = model.predict(data).reshape(ntot, -1)
    return scipy.special.expit(zetas[0] - zetas[1:])
