""" Work with gradient boosting tools. """
import numpy


def score(proba):
    """ Return the mean log likelihood ratio vs 50:50. """
    return numpy.mean(numpy.log(proba) - numpy.log(0.5))
